var btn = document.getElementById('btn');
var done = document.getElementById('done');
var confrm = document.getElementById('confirm');
confrm.style.display = 'none';
done.style.display = 'none';


var object = document.getElementById('swp'),
    initX, firstX;

btn.addEventListener('click', function() {
    document.getElementById('default').style.display = 'none';
    confrm.style.display = 'block';
});


object.addEventListener('mousedown', function(e) {

    e.preventDefault();
    initX = this.offsetLeft;
    firstX = e.pageX;

    this.addEventListener('mousemove', dragIt, false);

    window.addEventListener('mouseup', function() {
        object.removeEventListener('mousemove', dragIt, false);
        confrm.remove();
        done.style.display = 'block';

    }, false);

}, false);

function dragIt(e) {
    this.style.left = initX + e.pageX - firstX + 'px';
}

function getCssValue(id, props) {
    var lm = document.getElementById(id);
    return window.getComputedStyle(lm, null).getPropertyValue(props);
}